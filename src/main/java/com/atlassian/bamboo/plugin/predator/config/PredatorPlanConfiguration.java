package com.atlassian.bamboo.plugin.predator.config;

import com.atlassian.bamboo.build.BuildDefinition;
import com.atlassian.bamboo.build.Job;
import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.atlassian.bamboo.v2.build.BaseBuildConfigurationAwarePlugin;
import com.atlassian.bamboo.v2.build.configuration.MiscellaneousBuildConfigurationPlugin;
import com.atlassian.bamboo.ww2.actions.build.admin.create.BuildConfiguration;
import org.jetbrains.annotations.NotNull;

import java.util.Map;

/**
 * Adds a config panel to the miscellaneous section of the plan config with a boolean checkbox to disable predator
 */
public class PredatorPlanConfiguration extends BaseBuildConfigurationAwarePlugin implements MiscellaneousBuildConfigurationPlugin{

    @Override
    public boolean isApplicableTo(@NotNull Plan plan) {
        return (plan instanceof Plan && !(plan instanceof Job));
    }

    public void populateContextForView(@NotNull Map<String, Object> context, @NotNull Plan plan)
    {
        super.populateContextForView(context, plan);
    }

    public void populateContextForEdit(@NotNull Map<String, Object> context, @NotNull BuildConfiguration buildConfiguration, @NotNull Plan plan)
    {
        super.populateContextForEdit(context, buildConfiguration, plan);
    }

    public void transformBuildDefinition(@NotNull Map<String, Object> stringObjectMap, @NotNull Map<String, String> stringStringMap, @NotNull BuildDefinition buildDefinition)
    {

    }

    @NotNull
    @Override
    public ErrorCollection validate(@NotNull BuildConfiguration buildConfiguration)
    {
        ErrorCollection errors = super.validate(buildConfiguration);
        return errors;
    }
}
