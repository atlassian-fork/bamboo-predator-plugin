package com.atlassian.bamboo.plugin.predator.executor;

import java.io.IOException;
import java.util.concurrent.Callable;

public class ProcessWaiter implements Callable<Integer> {
    private final ProcessBuilder processBuilder;
    private Process process;

    public ProcessWaiter(ProcessBuilder processBuilder) {
        this.processBuilder = processBuilder;
    }

    public Integer call() throws InterruptedException, IOException {
        process = processBuilder.start();
        process.waitFor();

        return process.exitValue();
    }

    public void destroy() {
        if (null != process) {
            process.destroy();
        }
    }

    String command() {
        return String.valueOf(processBuilder.command());
    }
}
