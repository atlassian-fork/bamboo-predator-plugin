package com.atlassian.bamboo.plugin.predator.executor;


import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.build.logger.NullBuildLogger;
import com.atlassian.bamboo.v2.build.BuildContext;
import com.google.common.collect.Iterators;
import com.google.common.collect.Lists;
import com.jezhumble.javasysmon.JavaSysMon;
import com.jezhumble.javasysmon.ProcessInfo;
import org.apache.commons.io.FileSystemUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.tools.ant.taskdefs.condition.Os;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.doThrow;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;


@RunWith(PowerMockRunner.class)
@PrepareForTest({FileSystemUtils.class, FileUtils.class, System.class})
public class PredatorExecutorTest {

    @Mock
    private JavaSysMon javaSysMon;

    @Mock
    private BuildContext buildContext;

    @Mock
    private BuildLogger buildLogger = new NullBuildLogger();

    @Rule
    public TemporaryFolder mavenLocalRepoDirectory= new TemporaryFolder();

    @InjectMocks
    private PredatorExecutor predatorExecutor;

    @Test
    public void retrieveFreeDiskOneGB() throws Exception {
        mockStatic(FileSystemUtils.class);
        PowerMockito.doReturn(1024*1024L).when(FileSystemUtils.class, "freeSpaceKb");
        float freeDisk = predatorExecutor.retrieveFreeSpaceGB();
        assertEquals(freeDisk, 1f, 0.01);
    }

    @Test
    public void retrieveFreeDiskHalfGB() throws Exception {
        mockStatic(FileSystemUtils.class);
        PowerMockito.doReturn(512*1024L).when(FileSystemUtils.class, "freeSpaceKb");
        float freeDisk = predatorExecutor.retrieveFreeSpaceGB();
        assertEquals(freeDisk, 0.5f, 0.01);
    }

    @Test
    public void notFailIfDiskFreeFails() throws Exception {
        mockStatic(FileSystemUtils.class);
        doThrow(new IOException("failed")).when(FileSystemUtils.class, "freeSpaceKb");
        float freeDisk = predatorExecutor.retrieveFreeSpaceGB();
        assertEquals(freeDisk, Float.MAX_VALUE, 0.01);
    }

    @Test
    public void skipRemoveSnapshotsIfConfigured() throws Exception {
        mockStatic(FileUtils.class);
        predatorExecutor.removeMavenSnapshots(false, new File("/"));
        PowerMockito.verifyZeroInteractions(FileUtils.class);
    }

    @Test
    public void notCallDeleteSnaphotIfFolderDoesntExist() throws Exception {
        final File mavenLocalRepoDirectory = mock(File.class);
        when(mavenLocalRepoDirectory.isDirectory()).thenReturn(false);
        mockStatic(FileUtils.class);
        when(FileUtils.listFilesAndDirs(eq(mavenLocalRepoDirectory), (IOFileFilter) any(), (IOFileFilter) any())).
                thenReturn(Lists.<File>newArrayList());

        predatorExecutor.removeMavenSnapshots(true, mavenLocalRepoDirectory);

        PowerMockito.verifyStatic(never());
        FileUtils.listFilesAndDirs(eq(mavenLocalRepoDirectory), (IOFileFilter) any(), (IOFileFilter) any());

        PowerMockito.verifyZeroInteractions();
        FileUtils.deleteQuietly((File) any());

    }

    @Test
    public void deleteOnlySnapshotFolder() throws Exception {
        final File mavenLocalRepoDirectory = mock(File.class);
        when(mavenLocalRepoDirectory.isDirectory()).thenReturn(true);
        final File snapshotFolder = new File("4.6-SNAPSHOT");
        final File stableFolder = new File("4.6");
        final File randomFolder = new File("blabla");
        final List<File> listFolders = Lists.newArrayList(snapshotFolder, stableFolder, randomFolder);

        mockStatic(FileUtils.class);
        when(FileUtils.listFilesAndDirs(eq(mavenLocalRepoDirectory), (IOFileFilter) any(), (IOFileFilter) any())).
                thenReturn(listFolders);

        predatorExecutor.removeMavenSnapshots(true, mavenLocalRepoDirectory);

        PowerMockito.verifyStatic();
        FileUtils.deleteQuietly((File) any());

        PowerMockito.verifyStatic();
        FileUtils.deleteQuietly(snapshotFolder);

        PowerMockito.verifyStatic(never());
        FileUtils.deleteQuietly(stableFolder);

        PowerMockito.verifyStatic(never());
        FileUtils.deleteQuietly(randomFolder);

    }

    @Test
    public void notCallRemoveEmptyArtifactsIfRepositoryDoesntExist() throws Exception
    {
        final File mavenLocalRepoDirectory = mock(File.class);
        when(mavenLocalRepoDirectory.isDirectory()).thenReturn(false);
        mockStatic(FileUtils.class);
        when(FileUtils.listFilesAndDirs(eq(mavenLocalRepoDirectory), (IOFileFilter) any(), (IOFileFilter) any())).
                thenReturn(Lists.<File>newArrayList());

        predatorExecutor.removeEmptyArtifacts(mavenLocalRepoDirectory);

        PowerMockito.verifyStatic(never());
        FileUtils.listFilesAndDirs(eq(mavenLocalRepoDirectory), (IOFileFilter) any(), (IOFileFilter) any());

        PowerMockito.verifyZeroInteractions();
        FileUtils.deleteQuietly((File) any());
    }

    @Test
    public void deleteOnlyEmptyArtifacts() throws Exception
    {
        final File validFile = mavenLocalRepoDirectory.newFile("gotTheStuff");
        Files.write(validFile.toPath(), "blah blah blah".getBytes());

        final File emptyFile = mavenLocalRepoDirectory.newFile("gotNoStuff");

        if (!emptyFile.exists())
        {
            emptyFile.createNewFile();
        }

        predatorExecutor.removeEmptyArtifacts(mavenLocalRepoDirectory.getRoot());

        // verify that we're purging the evil, empty file
        assertTrue("Expected a file with data to not be deleted.", validFile.exists());
        assertFalse("Expected empty file to be deleted.", emptyFile.exists());
    }

    @Test
    public void skipRemoveStableIfConfigured() throws Exception {
        mockStatic(FileUtils.class);
        predatorExecutor.removeMavenStable(false, new File(""), 0f, 0f);

        PowerMockito.verifyZeroInteractions(FileUtils.class);
    }

    @Test
    public void notCallDeleteStableIfFolderDoesntExist() throws Exception {
        final File mavenLocalRepoDirectory = mock(File.class);
        when(mavenLocalRepoDirectory.isDirectory()).thenReturn(false);
        mockStatic(FileUtils.class);
        mockStatic(FileSystemUtils.class);
        PowerMockito.doReturn(512*1024L).when(FileSystemUtils.class, "freeSpaceKb");

        when(FileUtils.iterateFiles(eq(mavenLocalRepoDirectory), (IOFileFilter) any(), (IOFileFilter) any())).
                thenReturn(Iterators.<File>emptyIterator());

        predatorExecutor.removeMavenStable(true, mavenLocalRepoDirectory, 180f, 5*1024f);

        PowerMockito.verifyStatic(never());
        FileUtils.iterateFiles(eq(mavenLocalRepoDirectory), (IOFileFilter) any(), (IOFileFilter) any());

        PowerMockito.verifyZeroInteractions();
        FileUtils.deleteQuietly((File) any());

    }

    @Test
    public void skipRemoveBuildDirIfConfigured() throws Exception {
        mockStatic(FileUtils.class);

        ExecutorService deleteService = Executors.newSingleThreadExecutor();

        predatorExecutor.removeOldBuildWorkingDirectory(false, false, new File(""), "", new String[0], deleteService);

        //wait for delete thread to finish
        deleteService.awaitTermination(5, TimeUnit.SECONDS);

        PowerMockito.verifyZeroInteractions(FileUtils.class);
    }

    @Test
    public void notRunIfBuildDirJustHasDefaultFiles() throws Exception {
        mockStatic(FileUtils.class);
        File folder = mock(File.class);
        when(folder.listFiles()).thenReturn(new File[]{new File("_git-repositories-cache"),
                new File("_hg-repositories-cache"),
                new File("repositoryData")});

        ExecutorService deleteService = Executors.newSingleThreadExecutor();

        predatorExecutor.removeOldBuildWorkingDirectory(true, false, folder, "", new String[0], deleteService);


        //wait for delete thread to finish
        deleteService.awaitTermination(5, TimeUnit.SECONDS);

        PowerMockito.verifyZeroInteractions();
        FileUtils.deleteQuietly((File) any());
    }

    @Test
    @PrepareForTest({Os.class, FileUtils.class})
    public void dontDeleteCurrentDirectoryOrDefaultOnWindows() throws Exception {
        mockStatic(FileUtils.class);
        File folder = mock(File.class);
        mockStatic(Os.class);
        when(Os.isFamily(Os.FAMILY_WINDOWS)).thenReturn(Boolean.TRUE);

        final File todelete2 = new File("todelete-2");
        final File todelete1 = new File("todelete-1");
        when(folder.listFiles()).thenReturn(new File[]{todelete1,
                todelete2,
                new File("currentbuild"),
                new File("repositoryData"),
                new File("_git-repositories-cache"),
                new File("_hg-repositories-cache")
        });

        ExecutorService deleteService = Executors.newSingleThreadExecutor();

        predatorExecutor.removeOldBuildWorkingDirectory(true, false, folder, "currentbuild", new String[0], deleteService);

        //wait for delete thread to finish
        deleteService.awaitTermination(5, TimeUnit.SECONDS);

        PowerMockito.verifyStatic(times(2));
        FileUtils.moveDirectory((File) any(), (File) any());

        PowerMockito.verifyStatic();
        FileUtils.moveDirectory(eq(todelete1), (File) any());

        PowerMockito.verifyStatic();
        FileUtils.moveDirectory(eq(todelete2), (File) any());
    }

    @Test
    public void onlyDeleteArtifactsUpToLimit() throws Exception {
        final File mavenLocalRepoDirectory = mock(File.class);
        when(mavenLocalRepoDirectory.isDirectory()).thenReturn(true);
        mockStatic(FileUtils.class);
        mockStatic(FileSystemUtils.class);

        final File todelete1 = new File("todelete-1");
        final File todelete2 = new File("todelete-2");

        when(FileSystemUtils.freeSpaceKb()).thenReturn(1536*1024L);  // 1.5GB free

        when(FileUtils.iterateFiles(eq(mavenLocalRepoDirectory), (IOFileFilter) any(), (IOFileFilter) any())).
                thenReturn(Lists.newArrayList(todelete1, todelete2).iterator());
        when(FileUtils.sizeOf(todelete1)).thenReturn(FileUtils.ONE_GB);
        when(FileUtils.sizeOf(todelete2)).thenReturn(FileUtils.ONE_KB);

        predatorExecutor.removeMavenStable(true, mavenLocalRepoDirectory, 1f, 2*1024f);       // asking for 2GBs

        PowerMockito.verifyStatic();
        FileUtils.deleteQuietly(todelete1);

        PowerMockito.verifyZeroInteractions();
        FileUtils.deleteQuietly(todelete2);

    }

    @Test
    public void skipKillProcessesIfConfigured() throws Exception {
        predatorExecutor.killProcesses("", javaSysMon);

        verify(javaSysMon, times(0));
    }

    @Test
    public void shouldNotKillCurrentProcessOrParents() throws Exception {
        final String command = "command-stuff";

        ProcessInfo currentProcess = givenProcessInfo(3, 2, command, "dummy");

        ProcessInfo parent1 = givenProcessInfo(2, 1, command, "dummy");

        ProcessInfo parent2 = givenProcessInfo(1, 0, command, "dummy");

        ProcessInfo parent3 = givenProcessInfo(0, 0, command, "dummy");

        when(javaSysMon.currentPid()).thenReturn(3);
        when(javaSysMon.supportedPlatform()).thenReturn(true);

        when(javaSysMon.processTable()).thenReturn(new ProcessInfo[]{currentProcess, parent2, parent1, parent3});

        predatorExecutor.killProcesses(command);
        verify(javaSysMon, never()).killProcessTree(Mockito.anyInt(), Mockito.anyBoolean());
    }

    @Test
    public void killMultipleProcess() throws Exception {
        final String command = "command-stuff";
        final String user = "bamboo-agent";

        ProcessInfo currentProcess = givenProcessInfo(3, 0, command, user);

        ProcessInfo parent = givenProcessInfo(0, 0, command, user);

        ProcessInfo processToKill1 = givenProcessInfo(6, 0, command, user);

        ProcessInfo processToKill2 = givenProcessInfo(9, 0, command, user);

        when(javaSysMon.currentPid()).thenReturn(3);
        when(javaSysMon.supportedPlatform()).thenReturn(true);

        when(javaSysMon.processTable()).thenReturn(new ProcessInfo[]{currentProcess, parent, processToKill1, processToKill2});

        predatorExecutor.killProcesses(command, javaSysMon);
        verify(javaSysMon, times(1)).killProcessTree(6, false);
        verify(javaSysMon, times(1)).killProcessTree(9, false);
    }

    @Test
    public void shouldNotKillProcessIfCannotGetCommand() throws Exception {
        final String command = "command-stuff";
        final String user = "bamboo-agent";

        ProcessInfo currentProcess = givenProcessInfo(3, 0, command, user);

        ProcessInfo parent = givenProcessInfo(0, 0, command, user);

        ProcessInfo processToKill1 = givenProcessInfo(6, 0, "", user);

        ProcessInfo processToKill2 = givenProcessInfo(9, 0, command, user);


        when(javaSysMon.currentPid()).thenReturn(3);
        when(javaSysMon.supportedPlatform()).thenReturn(true);

        when(javaSysMon.processTable()).thenReturn(new ProcessInfo[]{currentProcess, parent, processToKill1, processToKill2});

        predatorExecutor.killProcesses(command, javaSysMon);
        verify(javaSysMon, never()).killProcessTree(6, false);
        verify(javaSysMon, times(1)).killProcessTree(9, false);
    }

    private ProcessInfo givenProcessInfo(int pid, int parentId, String command, String user)
    {
        ProcessInfo parent = mock(ProcessInfo.class);
        when(parent.getPid()).thenReturn(pid);
        when(parent.getParentPid()).thenReturn(parentId);
        when(parent.getOwner()).thenReturn(user);
        when(parent.getCommand()).thenReturn(command);
        return parent;
    }


    @Test
    public void dontKillProcessesFromOtherUsers() throws Exception {
        final String command = "command-stuff";
        final String user = "bamboo-agent";

        ProcessInfo currentProcess = givenProcessInfo(3, 0, command, user);

        ProcessInfo parent = givenProcessInfo(0, 0, command, user);

        ProcessInfo processToKill1 = givenProcessInfo(6, 0, command, user);

        ProcessInfo processToKeep = givenProcessInfo(9, 0, command, "other-user");


        when(javaSysMon.currentPid()).thenReturn(3);
        when(javaSysMon.supportedPlatform()).thenReturn(true);

        when(javaSysMon.processTable()).thenReturn(new ProcessInfo[]{currentProcess, parent, processToKill1, processToKeep});

        predatorExecutor.killProcesses(command, javaSysMon);
        verify(javaSysMon, times(1)).killProcessTree(6, false);
        verify(javaSysMon, times(0)).killProcessTree(9, false);
    }

    @Test
    public void skipExecuteCommandsIfConfigured() throws Exception {
        Process process = mock(Process.class);
        Runtime runtime = mock(Runtime.class);
        when(runtime.exec(anyString())).thenReturn(process);

        predatorExecutor.runCommands(new String[] {""}, false);

        verify(runtime, times(0)).exec(anyString());
    }

    @Test
    public void executeForEachCommand() throws Exception {
        ProcessWaiter waiter1 = mock(ProcessWaiter.class);
        when(waiter1.command()).thenReturn("command1");
        when(waiter1.call()).thenReturn(0);
        ProcessWaiter waiter2 = mock(ProcessWaiter.class);
        when(waiter2.command()).thenReturn("command2");
        when(waiter2.call()).thenReturn(0);

        predatorExecutor.runCommandWaiters(Lists.newArrayList(waiter1, waiter2), 1);

        verify(waiter1, times(1)).call();
        verify(waiter2, times(1)).call();
    }

    @Test
    public void executeCommandsAfterFailure() throws Exception {
        ProcessWaiter waiter1 = mock(ProcessWaiter.class);
        when(waiter1.command()).thenReturn("badCommand.exe");
        when(waiter1.call()).thenThrow(new IOException("No such file or directory"));
        ProcessWaiter waiter2 = mock(ProcessWaiter.class);
        when(waiter2.command()).thenReturn("noSuchScript.sh");
        when(waiter2.call()).thenThrow(new IOException("No such file or directory"));
        ProcessWaiter waiter3 = mock(ProcessWaiter.class);
        when(waiter3.command()).thenReturn("command2");
        when(waiter3.call()).thenReturn(0);

        predatorExecutor.runCommandWaiters(Lists.newArrayList(waiter1, waiter2, waiter3), 1);

        verify(waiter1, times(1)).call();
        verify(waiter2, times(1)).call();
        verify(waiter3, times(1)).call();
    }

    @Test
    public void enforceCommandBlocking() throws Exception {
        long start = System.nanoTime();
        ProcessWaiter pw1 = new ProcessWaiter(new ProcessBuilder("src/test/resources/sleep.sh"));
        ProcessWaiter pw2 = new ProcessWaiter(new ProcessBuilder("src/test/resources/sleep.bash"));
        predatorExecutor.runCommandWaiters(Lists.newArrayList(pw1, pw2), 5);
        long end = System.nanoTime();

        long elapsedSeconds = TimeUnit.NANOSECONDS.toSeconds((end - start));

        assertTrue(elapsedSeconds > 3);
        assertTrue(elapsedSeconds < 7);
    }

    @Test
    public void shouldRunInThisAgent() throws Exception {
        assertEquals(predatorExecutor.shouldRunInThisAgent("BlaBla", ".*bla"), true);
    }

    @Test
    public void shouldNotRunInThisAgent() throws Exception {
        assertEquals(predatorExecutor.shouldRunInThisAgent("BlaBla", "bla"), false);

    }

    @Test
    public void skipRemoveFilesIfConfigured() throws Exception {
        mockStatic(FileUtils.class);
        predatorExecutor.deleteFiles(new String[]{}, new File(""), true);

        PowerMockito.verifyZeroInteractions(FileUtils.class);
    }

    @Test
    public void deleteFiles() throws Exception {
        mockStatic(FileUtils.class);
        final String file1 = "/etc/stuff";
        final String file2 = "/etc/other";

        predatorExecutor.deleteFiles(new String[]{file1, file2}, new File(""), true);

        PowerMockito.verifyStatic();
        FileUtils.deleteQuietly(new File(file1));

        PowerMockito.verifyStatic();
        FileUtils.deleteQuietly(new File(file2));
    }

    @Test
    public void deleteFilesWithTilde() throws Exception {
        mockStatic(FileUtils.class);
        final String file1 = "~/stuff";
        final String file2 = "~/other/blabla/";

        final File homeDirectory = new File("/home/myuser/");
        predatorExecutor.deleteFiles(new String[]{file1, file2}, homeDirectory, true);

        PowerMockito.verifyStatic();
        FileUtils.deleteQuietly(new File("/home/myuser/stuff"));

        PowerMockito.verifyStatic();
        FileUtils.deleteQuietly(new File("/home/myuser/other/blabla"));
    }
}